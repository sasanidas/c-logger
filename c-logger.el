;;; c-logger.el --- log keyboard commands to buffer

;; Copyright (C) 2020 Fermin Munoz
;; Copyright (C) 2013 Nic Ferrier
;; Copyright (C) 2012 Le Wang
;; Copyright (C) 2004  Free Software Foundation, Inc.

;; Author: Michael Weber <michaelw@foldr.org>
;; Maintainer: Fermin MF <fmfs@posteo.net>
;; Version: 0.0.1
;; Keywords: help,log,command

;; URL: https://gitlab.com/sasanidas/command-log
;; Package-Requires: ((emacs "25") (cl-lib "0.5"))
;; License: GPL-3.0-or-later


;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; This add-on can be used to demo Emacs to an audience.  When
;; activated, keystrokes get logged into a designated buffer, along
;; with the command bound to them.

;; To enable, use e.g.:
;;
;; (require 'c-logger-mode)
;; (add-hook 'LaTeX-mode-hook 'c-logger-mode)
;;
;; To see the log buffer, call M-x clm/open-command-log-buffer.

;; The key strokes in the log are decorated with ISO9601 timestamps on
;; the property `:time' so if you want to convert the log for
;; screencasting purposes you could use the time stamp as a key into
;; the video beginning.

;;; Code:

(eval-when-compile (require 'cl))

(defvar c-logger-log-text t
  "A non-nil setting means text will be saved to the command log.")

(defvar c-logger-log-repeat nil
  "A nil setting means repetitions of the same command are merged into the single log line.")

(defvar c-logger-recent-history-string ""
  "This string will hold recently typed text.")

(defun c-logger--recent-history ()
  (setq c-logger-recent-history-string
	(concat c-logger-recent-history-string
		(buffer-substring-no-properties (- (point) 1) (point)))))

(add-hook 'post-self-insert-hook 'c-logger--recent-history)

(defun c-logger--zap-recent-history ()
  (unless (or (member this-original-command
		      c-logger--command-exceptions)
	      (eq this-original-command #'self-insert-command))
    (setq c-logger-recent-history-string "")))

(add-hook 'post-command-hook 'c-logger--zap-recent-history)

(defvar c-logger-time-string "%Y-%m-%dT%H:%M:%S"
  "The string sent to `format-time-string' when command time is logged.")

(defvar c-logger-logging-dir "~/log/"
  "Directory in which to store files containing logged commands.")

(defvar c-logger--command-exceptions
  '(nil self-insert-command backward-char forward-char
        delete-char delete-backward-char backward-delete-char
        backward-delete-char-untabify
        universal-argument universal-argument-other-key
        universal-argument-minus universal-argument-more
        beginning-of-line end-of-line recenter
        move-end-of-line move-beginning-of-line
        handle-switch-frame
        newline previous-line next-line)
  "A list commands which should not be logged, despite logging being enabled.
Frequently used non-interesting commands (like cursor movements) should be put here.")

(defvar c-logger--command-log-buffer nil
  "Reference of the currenly used buffer to display logged commands.")
(defvar c-logger--command-repetitions 0
  "Count of how often the last keyboard commands has been repeated.")
(defvar c-logger--last-keyboard-command nil
  "Last logged keyboard command.")


(defvar c-logger--command-indentation 11
  "*Indentation of commands in command log buffer.")

(defgroup c-logger nil
  "Customization for the command log."
  :group 'tools
  :prefix "c-logger-"
  :link '(url-link :tag "Repository" "https://gitlab.com/sasanidas/command-log"))

(defcustom c-logger-mode-auto-show nil
  "Show the command-log window or frame automatically."
  :group 'command-log
  :type 'boolean)

(defcustom c-logger-mode-window-size 40
  "The size of the command-log window."
  :group 'command-log
  :type 'integer)

(defcustom c-logger-mode-window-font-size 2
  "The font-size of the command-log window."
  :group 'command-log
  :type 'integer)

(defcustom c-logger-mode-key-binding-open-log "C-c o"
  "The key binding used to toggle the log window."
  :group 'command-log
  :type '(radio
          (const :tag "No key" nil)
          (key-sequence "C-c o"))) ;; this is not right though it works for kbd

(defcustom c-logger-mode-open-log-turns-on-mode nil
  "Does opening the command log turn on the mode?"
  :group 'command-log
  :type 'boolean)

(defcustom c-logger-mode-is-global nil
  "Does turning on c-logger-mode happen globally?"
  :group 'command-log
  :type 'boolean)

;;;###autoload
(define-minor-mode c-logger-mode
  "Toggle keyboard command logging."
  :init-value nil
  :lighter " c-logger"
  :keymap nil
  (if c-logger-mode
      (when (and
             c-logger-mode-auto-show
             (not (get-buffer-window c-logger--command-log-buffer)))
        (c-logger-open-command-log-buffer))
      ;; We can close the window though
      (c-logger-close-command-log-buffer)))

(define-global-minor-mode global-c-logger-mode c-logger-mode
  c-logger-mode)

(defun c-buffer--buffer-log-command-p (cmd &optional buffer)
  "Determines whether keyboard command CMD should be logged.
If non-nil, BUFFER specifies the buffer used to determine whether CMD should be logged.
If BUFFER is nil, the current buffer is assumed."
  (let ((val (if buffer
		 (buffer-local-value c-logger-mode buffer)
	       c-logger-mode)))
    (and (not (null val))
	 (null (member cmd c-logger--command-exceptions)))))

(defmacro c-logger--save-command-environment (&rest body)
  (declare (indent 0))
  `(let ((deactivate-mark nil) ; do not deactivate mark in transient
                                        ; mark mode
	 ;; do not let random commands scribble over
	 ;; {THIS,LAST}-COMMAND
	 (this-command this-command)
	 (last-command last-command))
     ,@body))

(defun c-logger-open-command-log-buffer (&optional arg)
  "Opens (and create, if non-existant) a buffer used for logging.
Keyboard commands.  If ARG is Non-nil, the existing command log buffer
is cleared."
  (interactive "P")
  (with-current-buffer
      (setq c-logger--command-log-buffer
            (get-buffer-create " *command-log*"))
    (text-scale-set 1))
  (when arg
    (with-current-buffer c-logger--command-log-buffer
      (erase-buffer)))
  (let ((new-win (split-window-horizontally
                  (- 0 c-logger-mode-window-size))))
    (set-window-buffer new-win c-logger--command-log-buffer)
    (set-window-dedicated-p new-win t)))

(defun c-logger-close-command-log-buffer ()
  "Close the command log window."
  (interactive)
  (with-current-buffer
      (setq c-logger--command-log-buffer
            (get-buffer-create " *command-log*"))
    (let ((win (get-buffer-window (current-buffer))))
      (when (windowp win)
        (delete-window win)))))

;;;###autoload
(defun c-logger-toggle-command-log-buffer (&optional arg)
  "Toggle the command log showing or not.
Optional ARG."
  (interactive "P")
  (when (and c-logger-mode-open-log-turns-on-mode
             (not c-logger-mode))
    (if c-logger-mode-is-global
        (global-c-logger-mode)
        (c-logger-mode)))
  (with-current-buffer
      (setq c-logger--command-log-buffer
            (get-buffer-create " *command-log*"))
    (let ((win (get-buffer-window (current-buffer))))
      (if (windowp win)
          (c-logger-close-command-log-buffer)
          ;; Else open the window
          (c-logger-open-command-log-buffer arg)))))

(defun c-logger--scroll-buffer-window (buffer &optional move-fn)
"Update `point' of windows containing BUFFER according to MOVE-FN.
If non-nil, MOVE-FN is called on every window which displays BUFFER.
If nil, MOVE-FN defaults to scrolling to the bottom, making the last line visible.

Scrolling up can be accomplished with:
\(c-logger--scroll-buffer-window buf (lambda () (goto-char (point-min))))"
  (let ((selected (selected-window))
	(point-mover (or move-fn
			 (function (lambda () (goto-char (point-max)))))))
    (walk-windows (function (lambda (window)
			      (when (eq (window-buffer window) buffer)
				(select-window window)
				(funcall point-mover)
				(select-window selected))))
		  nil t)))

(defmacro c-logger--with-command-log-buffer (&rest body)
  (declare (indent 0))
  `(when (and (not (null c-logger--command-log-buffer))
	      (buffer-name c-logger--command-log-buffer))
     (with-current-buffer c-logger--command-log-buffer
       ,@body)))

(defun c-logger--log-command (&optional cmd)
  "Hook into `pre-command-hook' to intercept command activation.
It requires CMD."
  (c-logger--save-command-environment
    (setq cmd (or cmd this-command))
    (when (c-buffer--buffer-log-command-p cmd)
      (c-logger--with-command-log-buffer
        (let ((current (current-buffer)))
          (goto-char (point-max))
          (cond ((and (not c-logger-log-repeat) (eq cmd c-logger--last-keyboard-command))
                 (incf c-logger--command-repetitions)
                 (save-match-data
                   (when (and (> c-logger--command-repetitions 1)
                              (search-backward "[" (line-beginning-position -1) t))
                     (delete-region (point) (line-end-position))))
                 (backward-char) ; skip over either ?\newline or ?\space before ?\[
                 (insert " [")
                 (princ (1+ c-logger--command-repetitions) current)
                 (insert " times]"))
                (t ;; (message "last cmd: %s cur: %s" last-command cmd)
                 ;; showing accumulated text with interleaved key presses isn't very useful
		 (when (and c-logger-log-text (not c-logger-log-repeat))
		   (if (eq c-logger--last-keyboard-command 'self-insert-command)
		       (insert "[text: " c-logger-recent-history-string "]\n")))
                 (setq c-logger--command-repetitions 0)
                 (insert
                  (propertize
                   (key-description (this-command-keys))
                   :time  (format-time-string c-logger-time-string (current-time))))
                 (when (>= (current-column) c-logger--command-indentation)
                   (newline))
                 (move-to-column c-logger--command-indentation t)
                 (princ (if (byte-code-function-p cmd) "<bytecode>" cmd) current)
                 (newline)
                 (setq c-logger--last-keyboard-command cmd)))
          (c-logger--scroll-buffer-window current))))))

(defun c-logger-command-log-clear ()
  "Clear the command log buffer."
  (interactive)
  (with-current-buffer c-logger--command-log-buffer
    (erase-buffer)))

(defun c-logger--save-log-line (start end)
  "Helper function for `c-logger-save-command-log' to export text properties.
It requires START and END."
  (save-excursion
    (goto-char start)
    (let ((time (get-text-property (point) :time)))
      (if time
	  (list (cons start (if time 
				(concat "[" (get-text-property (point) :time) "] ")
			      "")))))))

(defun c-logger-save-command-log ()
  "Save commands to today's log.
Clears the command log buffer after saving."
  (interactive)
  (save-window-excursion
    (set-buffer (get-buffer " *command-log*"))
    (goto-char (point-min))
    (let ((now (format-time-string "%Y-%m-%d"))
	  (write-region-annotate-functions '(c-logger--save-log-line)))
      (while (and (re-search-forward "^.*" nil t)
		  (not (eobp)))
	(append-to-file (line-beginning-position) (1+ (line-end-position)) (concat c-logger-logging-dir now))))
    (c-logger-command-log-clear)))

(add-hook 'pre-command-hook 'c-logger--log-command)

(eval-after-load 'c-logger-mode
  '(when c-logger-mode-key-binding-open-log
    (global-set-key
     (kbd c-logger-mode-key-binding-open-log)
     'c-logger-toggle-command-log-buffer)))

(provide 'c-logger-mode)

;;; c-logger.el ends here
